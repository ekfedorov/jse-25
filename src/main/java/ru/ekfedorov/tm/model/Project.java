package ru.ekfedorov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.entity.IWBS;

@Getter
@Setter
@NoArgsConstructor
public final class Project extends AbstractBusinessEntity implements IWBS {

    public Project(@Nullable final String name, @Nullable final String description) {
        this.name = name;
        this.description = description;
    }

}
