package ru.ekfedorov.tm.util;

import org.jetbrains.annotations.Nullable;

public interface ValidateUtil {

    static boolean isEmpty(@Nullable final String value) {
        return value == null || value.isEmpty();
    }

    static boolean notNullOrLessZero(@Nullable final Integer value) {
        return value == null || value < 0;
    }

}
