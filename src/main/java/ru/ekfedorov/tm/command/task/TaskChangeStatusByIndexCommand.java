package ru.ekfedorov.tm.command.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.api.service.ITaskService;
import ru.ekfedorov.tm.command.AbstractProjectCommand;
import ru.ekfedorov.tm.command.AbstractTaskCommand;
import ru.ekfedorov.tm.enumerated.Status;
import ru.ekfedorov.tm.exception.system.NullObjectException;
import ru.ekfedorov.tm.exception.system.NullProjectException;
import ru.ekfedorov.tm.exception.system.NullTaskException;
import ru.ekfedorov.tm.model.Project;
import ru.ekfedorov.tm.model.Task;
import ru.ekfedorov.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Optional;

public final class TaskChangeStatusByIndexCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Change task status by index.";
    }

    @SneakyThrows
    @Override
    public void execute() {
        if (serviceLocator == null) throw new NullObjectException();
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CHANGE TASK STATUS]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final ITaskService taskService = serviceLocator.getTaskService();
        @NotNull final Optional<Task> task = taskService.findOneByIndex(userId, index);
        if (!task.isPresent()) throw new NullTaskException();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusId = TerminalUtil.nextLine();
        @NotNull final Status status = Status.valueOf(statusId);
        @NotNull final Optional<Task> taskUpdated = taskService.changeStatusByIndex(userId, index, status);
        if (!taskUpdated.isPresent()) throw new NullTaskException();
    }

    @NotNull
    @Override
    public String name() {
        return "change-task-status-by-index";
    }

}
