package ru.ekfedorov.tm.exception.empty;

import ru.ekfedorov.tm.exception.AbstractException;

public final class ProjectIdIsEmptyException extends AbstractException {

    public ProjectIdIsEmptyException() throws Exception {
        super("Error! ProjectId is empty...");
    }

}
